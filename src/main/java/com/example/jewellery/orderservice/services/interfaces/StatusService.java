package com.example.jewellery.orderservice.services.interfaces;

import com.example.jewellery.orderservice.entities.Status;
import com.example.jewellery.orderservice.entities.StatusEnum;

public interface StatusService {

  Status getStatus(StatusEnum status);
}
