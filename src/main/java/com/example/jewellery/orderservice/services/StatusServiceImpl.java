package com.example.jewellery.orderservice.services;

import com.example.jewellery.orderservice.entities.Status;
import com.example.jewellery.orderservice.entities.StatusEnum;
import com.example.jewellery.orderservice.repositories.StatusRepository;
import com.example.jewellery.orderservice.services.interfaces.StatusService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StatusServiceImpl implements StatusService {

  private final StatusRepository statusRepository;

  @Override
  public Status getStatus(StatusEnum status) {
    return statusRepository.findByStatus(status);
  }
}
