package com.example.jewellery.orderservice.services.interfaces;

import com.example.jewellery.orderservice.entities.Order;
import com.example.jewellery.orderservice.entities.OrderedProducts;
import java.util.List;
import java.util.UUID;

public interface OrderedProductsService {

  OrderedProducts createOrderedProduct(UUID productId, double priceAtPayment, int count,
      Order order);

  List<OrderedProducts> getAllByOrder(UUID orderId);
}
