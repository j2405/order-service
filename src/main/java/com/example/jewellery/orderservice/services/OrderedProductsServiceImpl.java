package com.example.jewellery.orderservice.services;

import com.example.jewellery.orderservice.entities.Order;
import com.example.jewellery.orderservice.entities.OrderedProducts;
import com.example.jewellery.orderservice.repositories.OrderedProductsRepository;
import com.example.jewellery.orderservice.services.interfaces.OrderedProductsService;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderedProductsServiceImpl implements OrderedProductsService {

  private final OrderedProductsRepository orderedProductsRepository;

  @Override
  public OrderedProducts createOrderedProduct(UUID productId, double priceAtPayment, int count,
      Order order) {

    return orderedProductsRepository.save(
        OrderedProducts.builder().id(UUID.randomUUID()).productId(productId)
            .priceAtPayment(priceAtPayment).order(order).count(count).build());
  }

  @Override
  public List<OrderedProducts> getAllByOrder(UUID orderId) {
    return orderedProductsRepository.findAllByOrderId(orderId);
  }
}
