package com.example.jewellery.orderservice.services;

import com.example.jewellery.orderservice.dtos.CreateOrderDto;
import com.example.jewellery.orderservice.dtos.OrderFilterDto;
import com.example.jewellery.orderservice.entities.Order;
import com.example.jewellery.orderservice.entities.StatusEnum;
import com.example.jewellery.orderservice.repositories.OrderRepository;
import com.example.jewellery.orderservice.services.interfaces.AddressService;
import com.example.jewellery.orderservice.services.interfaces.OrderService;
import com.example.jewellery.orderservice.services.interfaces.OrderedProductsService;
import com.example.jewellery.orderservice.services.interfaces.StatusService;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.criteria.Predicate;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

  private final OrderRepository orderRepository;
  private final StatusService statusService;
  private final AddressService addressService;
  private final OrderedProductsService orderedProductsService;

  @Override
  public Page<Order> getAll(UUID userId, OrderFilterDto orderFilter) {
    var sort = Sort.by(Direction.fromString(orderFilter.getSortDirection()),
        orderFilter.getSortColumn());
    var pageRequest = PageRequest
        .of(orderFilter.getPage(), orderFilter.getPageSize(), sort);
    return orderRepository.findAll((root, query, builder) -> {
      List<Predicate> predicates = new ArrayList<>();

      if (userId != null) {
        predicates.add(builder.equal(root.get("createdBy"), userId));
      }

      if (orderFilter.getStatus() != null) {
        var status = root.join("status");
        predicates.add(builder.equal(status.get("status"), orderFilter.getStatus()));
      }

      if (StringUtils.isNotBlank(orderFilter.getOrderNumber())) {
        predicates
            .add(builder.like(root.get("orderNumber"), "%" + orderFilter.getOrderNumber() + "%"));
      }

      return builder.and(predicates.toArray(new Predicate[]{}));
    }, pageRequest);
  }

  @Override
  public String createCustomOrder(UUID customerCreatingOrderId, String description) {
    var newOrder = Order.builder().id(UUID.randomUUID()).createdBy(customerCreatingOrderId)
        .orderNumber(
            RandomStringUtils.randomAlphanumeric(10)).createdOn(
            LocalDateTime.now()).updatedOn(LocalDateTime.now()).status(statusService.getStatus(
            StatusEnum.CUSTOM))
        .description(description).build();
    orderRepository.save(newOrder);
    return newOrder.getOrderNumber();
  }


  @Override
  public String createOrder(UUID customerCreatingId, CreateOrderDto createOrderDto) {

    var newOrder = Order.builder().id(UUID.randomUUID()).createdBy(customerCreatingId).orderNumber(
        RandomStringUtils.randomAlphanumeric(10)).createdOn(
        LocalDateTime.now()).updatedOn(LocalDateTime.now()).status(statusService.getStatus(
        StatusEnum.RECEIVED)).description(createOrderDto.getDeliveryComment()).build();

    var savedOrder = orderRepository.save(newOrder);

    var orderAddresses = newOrder.getAddresses();
    if (createOrderDto.isHasShippingAddress()) {
      var billingAddress = addressService
          .createAddress(createOrderDto.getFirstName(), createOrderDto.getLastName(),
              createOrderDto.getPhone(), createOrderDto.getAddress(), createOrderDto.getAddress2(),
              false, savedOrder, createOrderDto.getCity(), createOrderDto.getCountry(),
              createOrderDto.getZip());
      orderAddresses.add(billingAddress);

      var shippingAddress = addressService
          .createAddress(createOrderDto.getShippingFirstName(),
              createOrderDto.getShippingLastName(),
              createOrderDto.getShippingPhone(), createOrderDto.getShippingAddress(),
              createOrderDto.getShippingAddress2(),
              true, savedOrder, createOrderDto.getShippingCity(),
              createOrderDto.getShippingCountry(), createOrderDto.getShippingZip());
      orderAddresses.add(shippingAddress);
    } else {
      var shippingAddress = addressService
          .createAddress(createOrderDto.getFirstName(), createOrderDto.getLastName(),
              createOrderDto.getPhone(), createOrderDto.getAddress(), createOrderDto.getAddress2(),
              true, savedOrder, createOrderDto.getCity(), createOrderDto.getCountry(),
              createOrderDto.getZip());
      orderAddresses.add(shippingAddress);
    }

    createOrderDto.getOrderedProducts().forEach(orderedProduct -> orderedProductsService
        .createOrderedProduct(orderedProduct.getProductId(), orderedProduct.getPriceAtPayment(),
            orderedProduct.getCount(), savedOrder));

    return newOrder.getOrderNumber();
  }

  @Override
  public void updateOrderStatus(UUID orderId, StatusEnum newStatus) {
    var order = orderRepository.getById(orderId);
    order.setStatus(statusService.getStatus(
        newStatus));
    order.setUpdatedOn(LocalDateTime.now());
    orderRepository.save(order);
  }
}
