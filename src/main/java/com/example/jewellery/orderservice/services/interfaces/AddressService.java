package com.example.jewellery.orderservice.services.interfaces;

import com.example.jewellery.orderservice.entities.Address;
import com.example.jewellery.orderservice.entities.Order;

public interface AddressService {

  Address createAddress(String firstName, String lastName, String phone, String address,
      String address2, boolean isShippingAddress, Order order,String city, String country, String zip);
}
