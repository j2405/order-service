package com.example.jewellery.orderservice.services;

import com.example.jewellery.orderservice.entities.Address;
import com.example.jewellery.orderservice.entities.Order;
import com.example.jewellery.orderservice.repositories.AddressRepository;
import com.example.jewellery.orderservice.services.interfaces.AddressService;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {

  private final AddressRepository addressRepository;

  @Override
  public Address createAddress(String firstName, String lastName, String phone, String address,
      String address2, boolean isShippingAddress,
      Order order,String city, String country, String zip) {
    var newAddress = Address.builder().id(UUID.randomUUID()).firstName(firstName).lastName(lastName)
        .address(address).phone(phone).shippingAddress(isShippingAddress).address2(address2).order(order)
        .city(city).zip(zip).country(country)
        .build();
    addressRepository.save(newAddress);
    return newAddress;
  }
}
