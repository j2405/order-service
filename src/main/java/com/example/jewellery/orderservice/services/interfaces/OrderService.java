package com.example.jewellery.orderservice.services.interfaces;

import com.example.jewellery.orderservice.dtos.CreateOrderDto;
import com.example.jewellery.orderservice.dtos.OrderFilterDto;
import com.example.jewellery.orderservice.entities.Order;
import com.example.jewellery.orderservice.entities.StatusEnum;
import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.Page;

public interface OrderService {

  Page<Order> getAll(UUID userId, OrderFilterDto orderFilter);

  String createCustomOrder(UUID customerCreatingId,String description);

  String createOrder(UUID customerCreatingId, CreateOrderDto createOrderDto);


  void updateOrderStatus(UUID orderId, StatusEnum newStatus);
}
