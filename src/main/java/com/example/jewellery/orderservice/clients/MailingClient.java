package com.example.jewellery.orderservice.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "mailing-service")
public interface MailingClient {
  @RequestMapping(method = RequestMethod.POST, value = "/api/mail/order")
  String sendOrderCreatedMail(@RequestBody String orderNumber);
}
