package com.example.jewellery.orderservice.adapters;

import com.example.jewellery.orderservice.dtos.OrderDto;
import com.example.jewellery.orderservice.dtos.StatusDto;
import com.example.jewellery.orderservice.entities.Order;
import com.example.jewellery.orderservice.entities.Status;

public class StatusAdapter {
  public static StatusDto convertToDto(Status status) {
    return StatusDto.builder().id(status.getId()).status(status.getStatus()).canCancelOrder(status.isCanCancelOrder()).build();
  }
}
