package com.example.jewellery.orderservice.adapters;

import com.example.jewellery.orderservice.dtos.AddressDto;
import com.example.jewellery.orderservice.dtos.OrderDto;
import com.example.jewellery.orderservice.entities.Address;
import com.example.jewellery.orderservice.entities.Order;
import java.util.List;
import java.util.stream.Collectors;

public class AddressAdapter {

  public static List<AddressDto> convertToDto(List<Address> addresses) {
    return addresses.stream().map(AddressAdapter::convertToDto).collect(Collectors.toList());
  }

  public static AddressDto convertToDto(Address address) {
    return AddressDto.builder()
        .firstName(address.getFirstName()).lastName(address.getLastName()).phone(address.getPhone())
        .address(address.getAddress()).address2(address.getAddress2())
        .shippingAddress(address.isShippingAddress())
        .city(address.getCity()).zip(address.getZip()).country(address.getCountry()).build();
  }
}
