package com.example.jewellery.orderservice.adapters;

import com.example.jewellery.orderservice.dtos.OrderDto;
import com.example.jewellery.orderservice.dtos.OrderedProductsDto;
import com.example.jewellery.orderservice.entities.Order;
import com.example.jewellery.orderservice.entities.OrderedProducts;
import java.util.List;
import java.util.stream.Collectors;

public class OrderedProductsAdapter {

  public static List<OrderedProductsDto> convertToDto(List<OrderedProducts> orders) {
    return orders.stream().map(OrderedProductsAdapter::convertToDto).collect(Collectors.toList());
  }

  public static OrderedProductsDto convertToDto(OrderedProducts order) {
    return OrderedProductsDto.builder().productId(order.getProductId()).count(order.getCount())
        .priceAtPayment(order.getPriceAtPayment())
        .build();
  }
}
