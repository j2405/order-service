package com.example.jewellery.orderservice.adapters;

import com.example.jewellery.orderservice.dtos.OrderDto;
import com.example.jewellery.orderservice.dtos.OrdersPage;
import com.example.jewellery.orderservice.entities.Order;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

public class OrderAdapter {

  public static List<OrderDto> convertToDto(List<Order> orders) {
    return orders.stream().map(OrderAdapter::convertToDto).collect(Collectors.toList());
  }

  public static OrderDto convertToDto(Order order) {
    return OrderDto.builder().id(order.getId()).createdOn(order.getCreatedOn())
        .status(StatusAdapter.convertToDto(order.getStatus())).updatedOn(order.getUpdatedOn())
        .orderNumber(order.getOrderNumber())
        .addresses(AddressAdapter.convertToDto(order.getAddresses()))
        .description(order.getDescription())
        .build();
  }

  public static OrdersPage convertToPage(Page<Order> domainPage) {

    return OrdersPage.builder().orders(convertToDto(domainPage.getContent()))
        .hasNext(domainPage.hasNext()).build();
  }

}
