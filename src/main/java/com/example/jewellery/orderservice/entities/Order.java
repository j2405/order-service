package com.example.jewellery.orderservice.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "orders")
public class Order implements Serializable {

  @Id
  @Type(type = "pg-uuid")
  @GeneratedValue
  private UUID id;

  @ManyToOne
  private Status status;

  @Column(nullable = false)
  private LocalDateTime createdOn;

  @Column(nullable = false)
  private LocalDateTime updatedOn;

  @Column(nullable = false)
  private UUID createdBy;

  @Column(nullable = false)
  private String orderNumber;

  @Column
  private String description;

  @OneToMany(mappedBy = "order" )
  @Builder.Default
  private List<Address> addresses = new ArrayList<>();
}
