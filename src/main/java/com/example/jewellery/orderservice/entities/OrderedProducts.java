package com.example.jewellery.orderservice.entities;

import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "ordered_products")
public class OrderedProducts implements Serializable {
  @Id
  @Type(type = "pg-uuid")
  @GeneratedValue
  private UUID id;

  @Column(nullable = false)
  private UUID productId;

  @Column(nullable = false)
  private int count;

  @Column(nullable = false)
  private double priceAtPayment;

  @ManyToOne
  private Order order;
}
