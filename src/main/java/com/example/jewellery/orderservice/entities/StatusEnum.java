package com.example.jewellery.orderservice.entities;

public enum StatusEnum {
  CUSTOM,
  RECEIVED,
  IN_PROGRESS,
  DELIVERED,
  CANCELLED;
}
