package com.example.jewellery.orderservice.dtos;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrdersPage {
  private List<OrderDto> orders;
  private boolean hasNext;
}
