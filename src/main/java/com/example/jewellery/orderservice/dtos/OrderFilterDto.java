package com.example.jewellery.orderservice.dtos;

import com.example.jewellery.orderservice.entities.StatusEnum;
import lombok.Data;

@Data
public class OrderFilterDto {
  private StatusEnum status;
  private String orderNumber;
  private String sortColumn;
  private String sortDirection;
  private int page;
  private int pageSize;
}
