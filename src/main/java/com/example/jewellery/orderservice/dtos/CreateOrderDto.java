package com.example.jewellery.orderservice.dtos;

import java.util.List;
import lombok.Data;

@Data
public class CreateOrderDto {
  private String firstName;
  private String lastName;
  private String phone;
  private String address;
  private String address2;
  private String zip;
  private String city;
  private String country;

  private boolean hasShippingAddress;
  private String shippingFirstName;
  private String shippingLastName;
  private String shippingPhone;
  private String shippingAddress;
  private String shippingAddress2;
  private String shippingZip;
  private String shippingCity;
  private String shippingCountry;

  private String deliveryComment;

  private List<OrderedProductsDto> orderedProducts;
}
