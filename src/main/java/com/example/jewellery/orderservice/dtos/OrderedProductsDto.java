package com.example.jewellery.orderservice.dtos;

import java.util.UUID;
import javax.persistence.Column;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrderedProductsDto {
  private UUID productId;
  private int count;
  private double priceAtPayment;
}
