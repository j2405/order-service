package com.example.jewellery.orderservice.dtos;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrderDto {
  private UUID id;
  private StatusDto status;
  private LocalDateTime createdOn;
  private LocalDateTime updatedOn;
  private String orderNumber;
  private String description;
  private List<AddressDto> addresses;
  private List<OrderedProductsDto> orderedProducts;
}
