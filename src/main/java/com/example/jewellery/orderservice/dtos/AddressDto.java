package com.example.jewellery.orderservice.dtos;

import java.util.UUID;
import javax.persistence.Column;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AddressDto {

  private String firstName;
  private String lastName;
  private String city;
  private String country;
  private String zip;
  private String phone;
  private String address;
  private String address2;
  private boolean shippingAddress;
}
