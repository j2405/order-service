package com.example.jewellery.orderservice.dtos;

import com.example.jewellery.orderservice.entities.StatusEnum;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StatusDto {
  private UUID id;
  private StatusEnum status;
  private boolean canCancelOrder;
}
