package com.example.jewellery.orderservice.repositories;

import com.example.jewellery.orderservice.entities.Order;
import com.example.jewellery.orderservice.entities.OrderedProducts;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderedProductsRepository extends JpaRepository<OrderedProducts, UUID> {

  List<OrderedProducts> findAllByOrderId(UUID orderId);
}
