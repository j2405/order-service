package com.example.jewellery.orderservice.repositories;

import com.example.jewellery.orderservice.entities.Address;
import com.example.jewellery.orderservice.entities.Order;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, UUID> {

}
