package com.example.jewellery.orderservice.endpoints;

import com.example.jewellery.orderservice.adapters.OrderAdapter;
import com.example.jewellery.orderservice.clients.MailingClient;
import com.example.jewellery.orderservice.dtos.CreateOrderDto;
import com.example.jewellery.orderservice.dtos.OrderDto;
import com.example.jewellery.orderservice.dtos.OrderFilterDto;
import com.example.jewellery.orderservice.dtos.OrdersPage;
import com.example.jewellery.orderservice.entities.StatusEnum;
import com.example.jewellery.orderservice.services.interfaces.OrderService;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/orders")
public class OrderEndpoint {

  private final OrderService orderService;
  private final MailingClient mailingClient;

  @GetMapping
  public ResponseEntity<OrdersPage> getAll(OrderFilterDto orderFilter) {
    //TODO should take ID from token
    return ResponseEntity.ok().body(OrderAdapter.convertToPage(orderService.getAll(null, orderFilter)));
  }

  @PostMapping("/custom")
  public ResponseEntity<String> createCustomOrder(@RequestBody String description) {
    //TODO should take ID from token
    return ResponseEntity.ok(orderService.createCustomOrder(UUID.randomUUID(),description));
  }

  @PostMapping
  public ResponseEntity<Void> createOrder(@RequestBody CreateOrderDto createOrderDto) {
    //TODO should take ID from token
    var newlyCreatedOrder = orderService.createOrder(UUID.randomUUID(), createOrderDto);
    mailingClient.sendOrderCreatedMail(newlyCreatedOrder);
    return ResponseEntity.ok().build();
  }

  @PutMapping("/{orderId}")
  public ResponseEntity<Void> updateOrderStatus(@PathVariable UUID orderId,
      @RequestParam StatusEnum status) {
    //TODO check if admin from token
    orderService.updateOrderStatus(orderId, status);
    return ResponseEntity.ok().build();
  }

  @GetMapping("/test")
  public String test() {

    return "test";
  }
}
