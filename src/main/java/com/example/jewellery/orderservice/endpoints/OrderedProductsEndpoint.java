package com.example.jewellery.orderservice.endpoints;

import com.example.jewellery.orderservice.adapters.OrderedProductsAdapter;
import com.example.jewellery.orderservice.dtos.OrderedProductsDto;
import com.example.jewellery.orderservice.services.interfaces.OrderedProductsService;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/orders/{orderId}/products")
public class OrderedProductsEndpoint {

  private final OrderedProductsService orderedProductsService;

  @GetMapping
  public ResponseEntity<List<OrderedProductsDto>> getAllProductsForOrder(@PathVariable UUID orderId){
    return ResponseEntity.ok(OrderedProductsAdapter.convertToDto(orderedProductsService.getAllByOrder(orderId)));
  }
}
