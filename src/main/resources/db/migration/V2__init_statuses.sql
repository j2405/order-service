INSERT INTO public.status
(id, can_cancel_order, status)
VALUES(gen_random_uuid (), false, 'CUSTOM');

INSERT INTO public.status
(id, can_cancel_order, status)
VALUES(gen_random_uuid (), false, 'ACTIVE');

INSERT INTO public.status
(id, can_cancel_order, status)
VALUES(gen_random_uuid (), true, 'RECEIVED');

INSERT INTO public.status
(id, can_cancel_order, status)
VALUES(gen_random_uuid (), false, 'DELIVERED');

INSERT INTO public.status
(id, can_cancel_order, status)
VALUES(gen_random_uuid (), false, 'IN_PROGRESS');

INSERT INTO public.status
(id, can_cancel_order, status)
VALUES(gen_random_uuid (), false, 'CANCELLED');