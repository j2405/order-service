package com.example.jewellery.orderservice.unit.services;

import com.example.jewellery.orderservice.entities.Status;
import com.example.jewellery.orderservice.entities.StatusEnum;
import com.example.jewellery.orderservice.repositories.StatusRepository;
import com.example.jewellery.orderservice.services.StatusServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class StatusServiceTest {
    @Mock
    private StatusRepository statusRepository;

    @InjectMocks
    private StatusServiceImpl statusService;

    @Test
    public void testFindByStatus() {
        var status = Status.builder().status(StatusEnum.RECEIVED).build();
        Mockito.when(statusRepository.findByStatus(Mockito.any(StatusEnum.class))).thenReturn(status);
        Assertions.assertThat(statusService.getStatus(StatusEnum.RECEIVED)).isEqualTo(status);
    }

}
