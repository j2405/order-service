package com.example.jewellery.orderservice.unit.services;

import com.example.jewellery.orderservice.entities.OrderedProducts;
import com.example.jewellery.orderservice.entities.Status;
import com.example.jewellery.orderservice.entities.StatusEnum;
import com.example.jewellery.orderservice.repositories.OrderedProductsRepository;
import com.example.jewellery.orderservice.repositories.StatusRepository;
import com.example.jewellery.orderservice.services.OrderedProductsServiceImpl;
import com.example.jewellery.orderservice.services.StatusServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;


@ExtendWith(MockitoExtension.class)
public class OrderedProductsServiceTest {

    @Mock
    private OrderedProductsRepository orderedProductsRepository;

    @InjectMocks
    private OrderedProductsServiceImpl orderedProductsServiceImpl;

    @Test
    public void testFindByOrder() {

        Mockito.when(orderedProductsRepository.findAllByOrderId(Mockito.any(UUID.class))).thenReturn(Arrays.asList(OrderedProducts.builder().id(UUID.randomUUID()).build()));
        var orders = orderedProductsServiceImpl.getAllByOrder(UUID.randomUUID());

        Assertions.assertThat(orders).hasSize(1);
    }

    @Test
    public void testCreateOrderedProduct() {
var newId = UUID.randomUUID();
        Mockito.when(orderedProductsRepository.save(Mockito.any())).thenReturn(OrderedProducts.builder().id(newId).build());
        var orderedProduct = orderedProductsServiceImpl.createOrderedProduct(UUID.randomUUID(),20.5,4,null);

        Assertions.assertThat(orderedProduct).isEqualTo(OrderedProducts.builder().id(newId).build());
    }

}
