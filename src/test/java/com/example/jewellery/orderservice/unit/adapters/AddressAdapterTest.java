package com.example.jewellery.orderservice.unit.adapters;

import com.example.jewellery.orderservice.adapters.AddressAdapter;
import com.example.jewellery.orderservice.adapters.StatusAdapter;
import com.example.jewellery.orderservice.dtos.AddressDto;
import com.example.jewellery.orderservice.dtos.StatusDto;
import com.example.jewellery.orderservice.entities.Address;
import com.example.jewellery.orderservice.entities.Status;
import com.example.jewellery.orderservice.entities.StatusEnum;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.UUID;

public class AddressAdapterTest {

    @Test
    public void testAddressConversion() {
        var newUuid = UUID.randomUUID();
        var address = AddressAdapter.convertToDto(
                Address.builder().id(newUuid).address("Plovdiv").address2("Georgi Petakov 5 street")
                        .shippingAddress(true).city("Plovdiv").country("BG").firstName("Velin").lastName("Dimitrov").zip("1234")
                        .build());
        Assertions.assertThat(address)
                .isEqualTo(
                        AddressDto.builder().address("Plovdiv").address2("Georgi Petakov 5 street")
                                .shippingAddress(true).city("Plovdiv").country("BG").firstName("Velin").lastName("Dimitrov").zip("1234")
                                .build());
    }
}
