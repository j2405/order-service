package com.example.jewellery.orderservice.unit.adapters;

import com.example.jewellery.orderservice.adapters.StatusAdapter;
import com.example.jewellery.orderservice.dtos.StatusDto;
import com.example.jewellery.orderservice.entities.Status;
import com.example.jewellery.orderservice.entities.StatusEnum;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.UUID;

public class StatusAdapterTest {

    @Test
    public void testStatusConversion() {
        var newUuid= UUID.randomUUID();
        var statusDto = StatusAdapter.convertToDto(
                Status.builder().id(newUuid).status(StatusEnum.RECEIVED).canCancelOrder(true)
                        .build());
        Assertions.assertThat(statusDto)
                .isEqualTo(
                        StatusDto.builder().id(newUuid).status(StatusEnum.RECEIVED).canCancelOrder(true).build());
    }
}
