package com.example.jewellery.orderservice.unit.adapters;

import com.example.jewellery.orderservice.adapters.AddressAdapter;
import com.example.jewellery.orderservice.adapters.OrderAdapter;
import com.example.jewellery.orderservice.adapters.StatusAdapter;
import com.example.jewellery.orderservice.dtos.OrderDto;
import com.example.jewellery.orderservice.entities.Address;
import com.example.jewellery.orderservice.entities.Order;
import com.example.jewellery.orderservice.entities.Status;
import com.example.jewellery.orderservice.entities.StatusEnum;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.UUID;

public class OrderAdapterTest {

    @Test
    public void testOrderConversion() {
        var newUuid= UUID.randomUUID();
        var createdOnDate = LocalDateTime.now();
        var updatedOnDate = LocalDateTime.now();

        var addresses = Arrays.asList(Address.builder().address("Plovdiv").address2("Georgi Petakov 5 street").build());
        var status = Status.builder().status(StatusEnum.CANCELLED).canCancelOrder(true).build();

        var order =      Order.builder().id(newUuid)
                .createdOn(createdOnDate)
                .status(  status)
                .updatedOn(updatedOnDate)
                .orderNumber("orderNumber test")
                .addresses( addresses)
                .description("test description")
                .build();


        var orderDto = OrderAdapter.convertToDto(order);
        Assertions.assertThat(orderDto)
                .isEqualTo(
                        OrderDto.builder().id(newUuid).createdOn(createdOnDate)
                                .status(StatusAdapter.convertToDto(status))
                                .updatedOn(updatedOnDate)
                                .orderNumber("orderNumber test")
                                .addresses(AddressAdapter.convertToDto(addresses))
                                .description("test description")
                                .build());
    }
}
