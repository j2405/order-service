package com.example.jewellery.orderservice.unit.services;

import com.example.jewellery.orderservice.dtos.CreateOrderDto;
import com.example.jewellery.orderservice.entities.Address;
import com.example.jewellery.orderservice.entities.Order;
import com.example.jewellery.orderservice.entities.Status;
import com.example.jewellery.orderservice.entities.StatusEnum;
import com.example.jewellery.orderservice.repositories.OrderRepository;
import com.example.jewellery.orderservice.repositories.StatusRepository;
import com.example.jewellery.orderservice.services.OrderServiceImpl;
import com.example.jewellery.orderservice.services.StatusServiceImpl;
import com.example.jewellery.orderservice.services.interfaces.AddressService;
import com.example.jewellery.orderservice.services.interfaces.OrderedProductsService;
import com.example.jewellery.orderservice.services.interfaces.StatusService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {
    @Mock
    private OrderRepository orderRepository;

    @Mock
    private StatusService statusService;

    @Mock
    private AddressService addressService;

    @Mock
    private OrderedProductsService orderedProductsService;

    @InjectMocks
    private OrderServiceImpl orderService;

    @Test
    public void testCreateOrder_billingAddressOnly() {
        var newOrder = new Order();
        when(orderRepository.save(any(Order.class))).thenReturn(newOrder);
        when(statusService.getStatus(any(StatusEnum.class))).thenReturn(new Status());
        when(addressService.createAddress(anyString(), anyString(), anyString(), anyString(), anyString(), anyBoolean(), Mockito.any(Order.class), anyString(), anyString(), anyString()))
                .thenReturn(new Address());
        orderService.createOrder(UUID.randomUUID(), prepareCreationDto(false));

        verify(statusService).getStatus(any(StatusEnum.class));
        verify(orderRepository).save(any(Order.class));
        verify(addressService).createAddress(anyString(), anyString(), anyString(), anyString(), anyString(), anyBoolean(), any(Order.class), anyString(), anyString(), anyString());
        verify(orderedProductsService, Mockito.never()).createOrderedProduct(any(UUID.class), anyDouble(), anyInt(), any(Order.class));
    }

    @Test
    public void testCreateOrder_billingShippingAddress() {
        var newOrder = new Order();
        when(orderRepository.save(any(Order.class))).thenReturn(newOrder);
        when(statusService.getStatus(any(StatusEnum.class))).thenReturn(new Status());
        when(addressService.createAddress(anyString(), anyString(), anyString(), anyString(), anyString(), anyBoolean(), Mockito.any(Order.class), anyString(), anyString(), anyString()))
                .thenReturn(new Address());
        orderService.createOrder(UUID.randomUUID(), prepareCreationDto(true));

        verify(statusService).getStatus(any(StatusEnum.class));
        verify(orderRepository).save(any(Order.class));
        verify(addressService,Mockito.times(2)).createAddress(anyString(), anyString(), anyString(), anyString(), anyString(), anyBoolean(), any(Order.class), anyString(), anyString(), anyString());
        verify(orderedProductsService, Mockito.never()).createOrderedProduct(any(UUID.class), anyDouble(), anyInt(), any(Order.class));
    }


    private CreateOrderDto prepareCreationDto(boolean shippingAddress) {
        var dto = new CreateOrderDto();
        dto.setHasShippingAddress(shippingAddress);
        dto.setOrderedProducts(new ArrayList<>());
        dto.setFirstName("");
        dto.setLastName("");
        dto.setAddress("");
        dto.setAddress2("");
        dto.setZip("");
        dto.setCity("");
        dto.setCountry("");
        dto.setDeliveryComment("");
        dto.setPhone("");
        dto.setShippingAddress("");
        dto.setShippingAddress2("");
        dto.setShippingZip("");
        dto.setShippingPhone("");
        dto.setShippingFirstName("");
        dto.setShippingLastName("");
        dto.setShippingCity("");
        dto.setShippingCountry("");
        return dto;
    }
}
