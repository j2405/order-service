package com.example.jewellery.orderservice.unit.services;


import com.example.jewellery.orderservice.entities.Address;
import com.example.jewellery.orderservice.entities.Order;
import com.example.jewellery.orderservice.repositories.AddressRepository;
import com.example.jewellery.orderservice.services.AddressServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

@ExtendWith(MockitoExtension.class)
public class AddressServiceTest {

    @Mock
    private AddressRepository addressRepository;

    @InjectMocks
    private AddressServiceImpl addressServiceImpl;


    @Test
    public void testCreateAddress() {
        Mockito.when(addressRepository.save(Mockito.any())).thenReturn(Mockito.any());
        var newAddress   = addressServiceImpl.createAddress("firstName", "lastName", "01564", "address",
                "address2", false,
        null,"Plovdiv", "BG", "4000");


        var addressToCompareWith = Address.builder().firstName("firstName").lastName("lastName")
                .address("address").phone("01564").shippingAddress(false).address2("address2").order(null)
                .city("Plovdiv").zip("4000").country("BG")
                .build();
        Assertions.assertThat(newAddress).isNotNull().usingRecursiveComparison().ignoringFields("id").isEqualTo(addressToCompareWith);
    }

}
