package com.example.jewellery.orderservice.unit.adapters;

import com.example.jewellery.orderservice.adapters.OrderedProductsAdapter;
import com.example.jewellery.orderservice.adapters.StatusAdapter;
import com.example.jewellery.orderservice.dtos.OrderedProductsDto;
import com.example.jewellery.orderservice.dtos.StatusDto;
import com.example.jewellery.orderservice.entities.OrderedProducts;
import com.example.jewellery.orderservice.entities.Status;
import com.example.jewellery.orderservice.entities.StatusEnum;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.UUID;

public class OrderedProductsAdapterTest {

    @Test
    public void testOrderedProductConversion() {
        var newUuid= UUID.randomUUID();
        var productId= UUID.randomUUID();
        var orderedProductsDto = OrderedProductsAdapter.convertToDto(
                OrderedProducts.builder().id(newUuid).productId(productId).count(5).priceAtPayment(203)
                        .build());
        Assertions.assertThat(orderedProductsDto)
                .isEqualTo(
                        OrderedProductsDto.builder().productId(productId).count(5).priceAtPayment(203).build());
    }
}
